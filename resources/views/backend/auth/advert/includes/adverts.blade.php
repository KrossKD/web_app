<div class="font-weight-bold font-3xl">@lang('labels.backend.access.adverts.table.pro_ads')</div>
<div class="table-resolve">
    <table class="table">
        <thead>
        <tr>
            <th>@lang('labels.backend.access.adverts.table.title')</th>
            <th>@lang('labels.backend.access.adverts.table.description')</th>
            <th>@lang('labels.backend.access.adverts.table.first_name')</th>
            <th>@lang('labels.backend.access.adverts.table.last_name')</th>
            <th>@lang('labels.backend.access.adverts.table.clicks')</th>
            <th>@lang('labels.backend.access.adverts.table.displays')</th>
            <th>@lang('labels.backend.access.adverts.table.cost')</th>
            <th>@lang('labels.backend.access.adverts.table.created')</th>
            <th>@lang('labels.backend.access.adverts.table.updated')</th>
            <th>@lang('labels.backend.access.adverts.table.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($promo as $ad)
            <tr>
                <td>{{ $ad->title }}</td>
                <td>{{ $ad->description }}</td>
                <td>{{ $ad->user->first_name }}</td>
                <td>{{ $ad->user->last_name }}</td>
                <td>{{ $ad->stats->sum('clicks')  }}</td>
                <td>{{ $ad->stats->sum('displays')  }}</td>
                <td>{{ $ad->stats->sum('clicks')*0.5 }} $</td>
                <td>{{ $ad->created_at->diffForHumans() }}</td>
                <td>{{ $ad->updated_at->diffForHumans() }}</td>
                <td>
                    <form action="{{ route('admin.auth.adverts.change.status', [$ad->user->id, $ad->id]) }}" method="POST">
                        @method('PATCH')
                        @csrf
                        @if($ad->suspended)
                            <button class="btn btn-primary" type="submit">@lang('labels.backend.access.adverts.table.show')</button>
                        @else
                            <button class="btn btn-danger" type="submit">@lang('labels.backend.access.adverts.table.suspend')</button>
                        @endif
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="font-weight-bold font-3xl">@lang('labels.backend.access.adverts.table.ads')</div>
<div class="table-resolve">
    <table class="table">
        <thead>
        <tr>
            <th>@lang('labels.backend.access.adverts.table.title')</th>
            <th>@lang('labels.backend.access.adverts.table.description')</th>
            <th>@lang('labels.backend.access.adverts.table.first_name')</th>
            <th>@lang('labels.backend.access.adverts.table.last_name')</th>
            <th>@lang('labels.backend.access.adverts.table.clicks')</th>
            <th>@lang('labels.backend.access.adverts.table.displays')</th>
            <th>@lang('labels.backend.access.adverts.table.cost')</th>
            <th>@lang('labels.backend.access.adverts.table.created')</th>
            <th>@lang('labels.backend.access.adverts.table.updated')</th>
            <th>@lang('labels.backend.access.adverts.table.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($adverts as $ad)
            <tr>
                <td>{{ $ad->title }}</td>
                <td>{{ $ad->description }}</td>
                <td>{{ $ad->user->first_name }}</td>
                <td>{{ $ad->user->last_name }}</td>
                <td>{{ $ad->stats->sum('clicks')  }}</td>
                <td>{{ $ad->stats->sum('displays')  }}</td>
                <td>{{ $ad->stats->sum('clicks')*0.5 }} $</td>
                <td>{{ $ad->created_at->diffForHumans() }}</td>
                <td>{{ $ad->updated_at->diffForHumans() }}</td>
                <td>
                    <form action="{{ route('admin.auth.adverts.change.status', [$ad->user->id, $ad->id]) }}" method="POST">
                        @method('PATCH')
                        @csrf
                        @if($ad->suspended)
                            <button class="btn btn-primary" type="submit">@lang('labels.backend.access.adverts.table.show')</button>
                        @else
                            <button class="btn btn-danger" type="submit">@lang('labels.backend.access.adverts.table.suspend')</button>
                        @endif
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

