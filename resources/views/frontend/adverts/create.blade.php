@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-pencil-alt"></i> @lang('navs.frontend.create-new-ad')
                    </strong>
                </div><!--card-header-->

                <div>
                    <form action="{{ route('frontend.user.adverts.store') }}" method="POST">
                        @csrf

                        <div class="m-3">
                            <label for="title">@lang('labels.frontend.advert.general.title') : </label>
                            <input type="text" name="title" value="{{ old('title') }}">
                        </div>
                        <div class="m-3">
                            <label for="description">@lang('labels.frontend.advert.general.description') </label>
                            <textarea class="description" name="description" ></textarea>
                            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                            <script>
                                tinymce.init({
                                    selector:'textarea.description',
                                    width: 900,
                                    height: 300
                                });
                            </script>
                        </div>
                        <button type="submit" class="btn btn-primary m-3">@lang('labels.general.buttons.save') </button>
                    </form>
                </div>

            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
