@extends('frontend.layouts.app')


@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-pencil-alt"></i> @lang('navs.frontend.ads')
                    </strong>

                    <a class="btn btn-primary float-right" href="{{ route('frontend.user.adverts.create') }}">@lang('labels.frontend.advert.general.create_ad')</a>
                </div><!--card-header-->

                <div class="card-body">

                    @include('includes.partials.adverts.edit', $adverts)


                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
