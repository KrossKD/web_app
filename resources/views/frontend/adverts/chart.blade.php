@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<div class="cost">
    <div class="border border-secondary rounded bg-info text-light d-inline-block p-1">
        @lang('labels.frontend.advert.chart.cost', ['cost' => $cost])
    </div>
</div>
<div class="chart">

    {!! $chart->container() !!}
    {!! $chart->script() !!}
</div>
<hr>
<form action="{{ route('frontend.user.adverts.edit', [$advert->id]) }}" method="GET">
    <button class="btn btn-primary" type="submit">@lang('labels.frontend.advert.chart.back')</button>
</form>

@endsection
