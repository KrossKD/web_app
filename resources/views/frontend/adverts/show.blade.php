@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="font-weight-bold">
                            {{ $advert->title }}
                            @if($advert->suspended)
                                @include('includes.partials.adverts.suspended')
                            @endif
                        </div>
                        <div class="ml-auto">
                            <a href="{{ route('frontend.user.profile.show', [$advert->user_id]) }}" class="text-decoration-none">
                                {{ $advert->user->first_name }}
                                {{ $advert->user->last_name }}
                            </a>

                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        {{ $advert->description }}
                    </div>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->

@endsection
