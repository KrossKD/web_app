@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    @lang('labels.frontend.advert.general.edit_ad')
                    @if($advert->suspended)
                        @include('includes.partials.adverts.suspended')
                    @endif
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="card-body">
                            <form action="{{ route('frontend.user.adverts.update', [$advert->id]) }}" method="POST">
                                @method('PATCH')
                                @csrf

                                <div class="m-3">
                                    <label for="title">@lang('labels.frontend.advert.general.title') : </label>
                                    <input type="text" name="title" value="{{ $advert->title }}">
                                </div>
                                <div class="m-3">
                                    <label for="description">@lang('labels.frontend.advert.general.description') : </label>
                                    <textarea class="description" name="description">{{ $advert->description }}</textarea>
                                    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                    <script>
                                        tinymce.init({
                                            selector:'textarea.description',
                                            height: 300
                                        });
                                    </script>
                                </div>
                                <div class="m-3">
                                    <label for="promoted">@lang('labels.frontend.advert.general.promote') </label>
                                    <input type="checkbox" name="promoted" {{ $advert->promoted ? 'checked' : '' }}>
                                </div>
                                <button type="submit" class="btn btn-primary m-3">@lang('labels.general.buttons.update')</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card m-2">
                            <div class="card-header py-1">
                                @lang('labels.frontend.advert.general.stats')
                            </div>
                            <div class="card-body">
                                @include('includes.partials.adverts.stats')
                            </div>
                        </div>
                    </div>
                </div>

            </div><!--card-->
        </div><!--col-->
    </div><!--row-->

@endsection
