@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    @lang('labels.frontend.advert.general.pro_ads')
                </div>
                <div class="card-body">
                    @include('includes.partials.adverts.promo')
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->

    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    @lang('labels.frontend.advert.general.ads')
                </div>
                <div class="card-body">

                        <div class="table-resolve">
                            <table class="table">
                                <tbody>
                                @foreach($adverts as $ad)
                                    <tr>
                                        <td><a href="{{ route('frontend.user.adverts.show', [$ad->id]) }}">{{ $ad->title }}</a></td>
                                        <td>{{ $ad->description }}</td>
                                        <td>{{ $ad->user->first_name }}</td>
                                        <td>{{ $ad->user->last_name }}</td>
                                        @can('view backend')
                                            <td>
                                                <form action="{{ route('admin.auth.adverts.index', [$ad->user->id]) }}" method="GET">
                                                    <button class="btn btn-primary" type="submit">@lang('labels.frontend.advert.general.edit')</button>
                                                </form>
                                            </td>
                                            <td>
                                                @if( $ad->suspended )
                                                    <form action="{{ route('admin.auth.adverts.change.status', [$ad->user->id, $ad->id]) }}" method="POST">
                                                        @method('PATCH')
                                                        @csrf

                                                        <button class="btn btn-primary" type="submit">@lang('labels.frontend.advert.general.show')</button>
                                                    </form>
                                                @else
                                                    <form action="{{ route('admin.auth.adverts.change.status', [$ad->user->id, $ad->id]) }}" method="POST">
                                                        @method('PATCH')
                                                        @csrf

                                                        <button class="btn btn-danger" type="submit">@lang('labels.frontend.advert.general.suspend')</button>
                                                    </form>
                                                @endif
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection
