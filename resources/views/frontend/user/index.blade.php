@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <div class="card">
                                <div class="card-body">
                                    @foreach($adverts as $ad)
                                        @if($ad->promoted)
                                            <div class="border border-secondary rounded-pill pl-4 mb-2 bg-warning">
                                        @elseif($ad->suspended)
                                            <div class="border border-secondary rounded-pill pl-4 mb-2 bg-danger text-white">
                                        @else
                                            <div class="border border-secondary rounded-pill pl-4 mb-2">
                                        @endif

                                            <div class="row">
                                                <div class="col-8">
                                                    <a href="{{ route('frontend.user.adverts.show', [$ad->id]) }}" class="text-decoration-none">{{ $ad->title }}</a>
                                                </div>
                                                <div class="col-4">
                                                    {{ $ad->updated_at->format('d/m/Y H:i:s') }}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card mb-4 bg-light">
                                <img class="card-img-top" src="{{ $user->picture }}" alt="Profile Picture">

                                <div class="card-body">
                                    <h4 class="card-title">
                                        {{ $user->name }}<br/>
                                    </h4>

                                    <p class="card-text">
                                        <small>
                                            <i class="fas fa-envelope"></i> {{ $user->email }}<br/>
                                            <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined') {{ timezone()->convertToLocal($user->created_at, 'F jS, Y') }}
                                        </small>
                                    </p>

                                    @if($user == $logged_in_user)
                                        <p class="card-text">
                                            <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm mb-1">
                                                <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
                                            </a>

                                            <a href="{{ route('frontend.user.adverts.index')}}" class="btn btn-info btn-sm mb-1 ml-1">
                                                <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.adverts')
                                            </a>
                                                @can('view backend')
                                                    &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-danger btn-sm mb-1">
                                                        <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
                                                    </a>
                                                @endcan
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
