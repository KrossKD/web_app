<form action="{{ route('frontend.user.adverts.stats', [$advert->id]) }}" method="GET">
    @csrf
    <div>
        <div class="row">
            <div class="col-6">
                <label for="start">@lang('labels.frontend.advert.chart.from'): </label>
                <input type="date" name="start" value="{{ date("Y-m-d") }}">
            </div>
            <div class="col-6">
                <label for="end">@lang('labels.frontend.advert.chart.to'): </label>
                <input type="date" name="end" value="{{ date("Y-m-d") }}">
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-success m-2">@lang('labels.frontend.advert.chart.load')</button>
        </div>
    </div>
</form>
