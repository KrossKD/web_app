
<div class="row">
    @foreach($promo as $pro_ad)
        <div class="col-4">
            <div class="border border-secondary rounded-pill mb-2 py-2 pl-4 bg-warning">
                <div>
                    <a href="{{ route('frontend.user.adverts.show', [$pro_ad->id]) }}" class="font-weight-bold text-dark text-decoration-none">{{ $pro_ad->title }}</a>

                    @can('view backend')
                        <span class="float-right mr-4">
                            <form action="{{ route('admin.auth.adverts.index', [$pro_ad->user->id]) }}" method="GET">
                                <button class="btn btn-primary btn-sm" type="submit">@lang('labels.frontend.advert.general.edit')</button>
                            </form>
                        </span>
                    @endcan
                </div>
                <div class="pt-2">
                    {{ $pro_ad->description }}
                </div>
            </div>
        </div>
    @endforeach
</div>
