<span class="text-light bg-danger border rounded p-2 ml-4" title="@lang('labels.frontend.advert.general.suspend_info')">
    @lang('labels.frontend.advert.general.suspend_alert')
</span>
