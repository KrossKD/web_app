@foreach($adverts as $ad)
    @if($ad->suspended)
        <div class="border border-secondary rounded mb-2 py-2 bg-secondary text-white">
    @else
        <div class="border border-secondary rounded mb-2 py-2">
    @endif
        <div class="row">
            <div class="col-6 pl-4">
                {{ $ad->title }}
            </div>
            <div class="col-2 d-flex">
                <a href="{{ route('frontend.user.adverts.edit', [$ad->id]) }}" type="button" class="btn btn-warning btn-sm mr-2">@lang('labels.frontend.advert.general.edit')</a>
                @include('includes.partials.adverts.delete')
            </div>
            <div class="col-4">{{ $ad->updated_at->format('d/m/Y H:i:s') }}</div>
        </div>
    </div>
@endforeach

