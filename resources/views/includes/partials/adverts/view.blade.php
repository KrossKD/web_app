
    @foreach($adverts as $ad)
        <div class="border border-secondary rounded-pill mb-2 py-2">
            <div class="row">
                <div class="col-6 pl-4">
                    <a href="{{ route('frontend.user.adverts.show', [$ad->id]) }}">{{ $ad->title }}</a>
                </div>
                <div class="col-6">
                    {{ $ad->created_at }}
                </div>
            </div>
        </div>
    @endforeach
