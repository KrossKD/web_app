<form action="{{ route('frontend.user.profile.show', [$ad->id]) }}" method="POST">
    @method('DELETE')
    @csrf

    <button class="btn btn-danger btn-sm">@lang('labels.frontend.advert.general.delete')</button>
</form>
