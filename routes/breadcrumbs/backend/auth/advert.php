<?php

Breadcrumbs::for('admin.auth.adverts.index', function ($trail, $id) {
    $trail->parent('admin.auth.adverts.index');
    $trail->push(__('menus.backend.access.users.edit'), route('admin.auth.adverts.index', $id));
});
