<?php

use App\Http\Controllers\AdvertsController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::get('profile/{profile}', [ProfileController::class, 'index'])->name('profile.show');
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');

        // Advert Specific
        Route::get('adverts', [AdvertsController::class, 'index'])->name('adverts.index');
        Route::get('adverts/create', [AdvertsController::class, 'create'])->name('adverts.create');
        Route::post('adverts', [AdvertsController::class, 'store'])->name('adverts.store');
        Route::get('adverts/{advert}', [AdvertsController::class, 'show'])->name('adverts.show');
        Route::get('adverts/{advert}/edit', [AdvertsController::class, 'edit'])->name('adverts.edit');
        Route::patch('adverts/{advert}', [AdvertsController::class, 'update'])->name('adverts.update');
        Route::delete('adverts/{advert}', [AdvertsController::class, 'destroy'])->name('adverts.destroy');

        // Advert Statistics
        Route::get('adverts/{advert}/stats', [AdvertsController::class, 'stats'])->name('adverts.stats');

    });
});
