<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $guarded = [];
    public function adverts()
    {
        $this->hasMany(Advert::class);
    }
}
