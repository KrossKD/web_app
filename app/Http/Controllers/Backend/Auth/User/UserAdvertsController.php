<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use App\Models\Auth\Advert;
use App\Models\Auth\User;
use Illuminate\Http\Request;

class UserAdvertsController extends Controller
{
    public function index(User $user)
    {
        return view('backend.auth.user.adverts')->withUser($user)->with('adverts', $user->adverts);
    }

    public function changeStatus(User $user, Advert $advert)
    {
        $advert->suspended = !$advert->suspended;
        $advert->save();

        return back();
    }
}
