<?php

namespace App\Http\Controllers\Backend\Auth\Advert;

use App\Http\Controllers\Controller;
use App\Models\Auth\Advert;
use App\Models\Auth\User;

class AdvertController extends Controller
{
    public function index(Advert $adverts, User $user)
    {
        $adverts = Advert::wherePromoted('0')->get();
        $promo = Advert::wherePromoted('1')->get();
        return view('backend.auth.user.adverts', compact('promo', 'adverts', 'user'));
    }
}
