<?php

namespace App\Http\Controllers\Frontend;

use App\Events\AdDisplayed;
use App\Http\Controllers\Controller;
use App\Models\Auth\Advert;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $adverts = Advert::wherePromoted('0')->whereSuspended('0')->get();
        $promo = Advert::wherePromoted('1')->whereSuspended('0')->OrderBy('updated_at', 'DESC')->take(3)->get();

        event(new AdDisplayed($promo));

        return view('frontend.user.main', compact('adverts', 'promo'));
    }
}
