<?php

namespace App\Http\Controllers;

use App\Events\AdVisited;
use App\Models\Auth\Advert;
use App\Models\Auth\Stat;
use Carbon\CarbonPeriod;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Carbon\Carbon;

class AdvertsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('frontend.adverts.index', [
            'adverts' => auth()->user()->adverts
        ]);
    }

    public function create()
    {
        return view('frontend.adverts.create');
    }

    public function store()
    {
        $attributes = request()->validate([
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:6']
        ]);
        $attributes['user_id'] = auth()->id();

        Advert::create($attributes);

        return redirect('/adverts');
    }

    public function show(Advert $advert)
    {
        event(new AdVisited($advert));

        return view('frontend.adverts.show', compact('advert'));
    }

    public function edit(Advert $advert)
    {
        if($advert->user_id != auth()->id())
        {
            abort(403);
        }
        //$this->authorize('edit', $advert); //doesn't work

        return view('frontend.adverts.edit', compact('advert'));
    }

    public function update(Advert $advert)
    {
        if($advert->user_id != auth()->id())
        {
           abort(403);
        }
        //$this->authorize('update', $advert); //doesn't work

        request()->validate([
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:6']
        ]);

        $advert->title = request('title');
        $advert->description = request('description');
        $advert->promoted = request()->has('promoted');

        $advert->save();

        return redirect('/adverts');
    }

    public function destroy(Advert $advert)
    {
        if($advert->user_id != auth()->id())
        {
            abort(403);
        }
        //$this->authorize('destroy', $advert); //doesn't work

        $advert->delete();

        return redirect('/adverts');
    }

    public function stats(Advert $advert)
    {
        request()->validate([
            'start' => ['required', 'after:2018-01-01'],
            'end' => ['required', 'after_or_equal:start', 'before:tomorrow'],
        ]);
        $date_start = Carbon::parse(request('start'));
        $date_end = Carbon::parse(request('end'));

        $period = CarbonPeriod::create($date_start, $date_end);
        $labels = [];
        $clicks = [];
        $displays = [];
        $cost = 0;
        $i = 0;
        foreach($period as $date)
        {
            $stat = Stat::whereDate('created_at', '=', $date)
                ->where('advert_id', '=', $advert->id)
                ->first();
            if($stat)
            {
                $clicks[$i] = $stat->clicks;
                $displays[$i] = $stat->displays;
                $cost += $clicks[$i];
            }
            else
            {
                $clicks[$i] = 0;
                $displays[$i] = 0;
            }
            $labels[$i] = $date->format('Y-m-d');
            $i++;
        }
        // transform nr clicks to cost (currency)
        $cost = $cost * 0.5;

        $chart = new Chart();
        $chart->title("Statistics for: " . $advert->title );
        $chart->labels($labels);
        $chart->dataset('Clicks', 'line', $clicks)->options([
            'borderColor' => '#FF5733',
            'fill' => '0',
            'lineTension' => '0.1'
        ]);
        $chart->dataset('Displays', 'line', $displays)->options([
            'borderColor' => '#335BFF',
            'fill' => '0',
            'lineTension' => '0.1'
        ]);

        return view('frontend.adverts.chart', compact('advert', 'chart', 'cost'));
    }
}
