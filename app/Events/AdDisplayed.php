<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class AdDisplayed
{
    use Dispatchable, SerializesModels;

    public $advert;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($advert)
    {
        $this->advert = $advert;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
}
