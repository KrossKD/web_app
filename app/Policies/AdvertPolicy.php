<?php

namespace App\Policies;

use App\Models\Auth\User;
use App\Advert;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvertPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the advert.
     *
     * @param  \App\Models\Auth\User  $user
     * @param  \App\Advert  $advert
     * @return mixed
     */
    public function view(User $user, Advert $advert)
    {
        //
    }

    /**
     * Determine whether the user can create adverts.
     *
     * @param  \App\Models\Auth\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the advert.
     *
     * @param  \App\Models\Auth\User  $user
     * @param  \App\Advert  $advert
     * @return mixed
     */
    public function edit(User $user, Advert $advert)
    {
        return $advert->user_id == $user->id;
    }

    public function update(User $user, Advert $advert)
    {
        return $advert->user_id == $user->id;
    }

    /**
     * Determine whether the user can delete the advert.
     *
     * @param  \App\Models\Auth\User  $user
     * @param  \App\Advert  $advert
     * @return mixed
     */
    public function delete(User $user, Advert $advert)
    {
        return $advert->user_id == $user->id;
    }

    /**
     * Determine whether the user can restore the advert.
     *
     * @param  \App\Models\Auth\User  $user
     * @param  \App\Advert  $advert
     * @return mixed
     */
    public function restore(User $user, Advert $advert)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the advert.
     *
     * @param  \App\Models\Auth\User  $user
     * @param  \App\Advert  $advert
     * @return mixed
     */
    public function forceDelete(User $user, Advert $advert)
    {
        //
    }
}
