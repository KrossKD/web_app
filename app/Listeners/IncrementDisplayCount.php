<?php

namespace App\Listeners;

use App\Events\AdDisplayed;
use App\Models\Auth\Stat;

class IncrementDisplayCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdDisplayed  $event
     * @return void
     */
    public function handle(AdDisplayed $event)
    {
        $adverts = $event->advert;
        foreach ($adverts as $ad)
        {
            $stat = Stat::where('advert_id', '=', $ad->id)->whereDate('created_at', '=', date('Y-m-d'))->first();
            if($ad->promoted)
            {
                if($stat)
                {
                    //update existent stats
                    ++$stat->displays;
                }
                else
                {
                    //create new stat record
                    $stat = new Stat();
                    $stat['advert_id'] = $ad->id;
                    ++$stat->displays;
                }

                $stat->save();
            }
        }
    }
}
