<?php

namespace App\Listeners;

use App\Events\AdVisited;
use App\Models\Auth\Stat;

class IncrementVisitCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdVisited  $event
     * @return void
     */
    public function handle(AdVisited $event)
    {
        $ad = $event->advert;
        //check if stats for present day exists
        $stat = Stat::where('advert_id', '=', $ad->id)->whereDate('created_at', '=', date('Y-m-d'))->first();
        if($ad->promoted)
        {
            if($stat)
            {
                //update existent stats
                ++$stat->clicks;
            }
            else
            {
                //create new stat record
                $stat = new Stat();
                $stat['advert_id'] = $ad->id;
                ++$stat->clicks;
            }

            $stat->save();
        }
    }
}
